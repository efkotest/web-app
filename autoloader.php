<?php
$path = dirname(__FILE__);

require_once($path . '/classes/helpers/jwt_helper.php');

require_once($path . '/config/auth.php');
require_once($path . '/config/pages.php');
require_once($path . '/config/app.php');

require_once($path . '/classes/Utils.php');
require_once($path . '/classes/User.php');
require_once($path . '/classes/PageManager.php');
require_once($path . '/classes/Api.php');