<?php

class PageManager extends User {

  private $defaultPage = 'authorization';
  private $page = null;
  private $pages = null;

  /**
   * PageManager constructor.
   * @param null $page Set to `null` to render default page
   */
  public function __construct($page = null) {
    $this->pages = $GLOBALS['PAGES_CONFIG'];
    $this->page = $page ?? $this->defaultPage;
    if (empty($this->pages[$this->page]) === TRUE) {
      header('Location: /');
      die();
    }

    if ($this->pages[$this->page]['private'] === TRUE) {
      if (parent::isAuthorized() === FALSE) {
        header('Location: /authorization');
        die();
      }
    }
  }

  public function render() {
    return $this->getContent();
  }

  private function getContent() {
    ob_start();
    require_once('./pages/'.$this->page.'.php');
    return ob_get_clean();
  }

}