<?php

class User extends Utils {

  private $jwtPayload = null;

  public function isAuthorized() {
    $payload = $this->getPayload();

    return $payload && $payload->userid >= 1;
  }

  public function getPayload() {
    if (!$this->jwtPayload) {
      $this->jwtPayload = parent::getJwtPayload($this->getJwt());
    }
    return $this->jwtPayload;
  }

  public function isAbbleTo($permission) {
    return in_array($permission, $this->getPermissions());
  }

  private function getJwt() {
    return $_COOKIE[AUTH_JWT_COOKIENAME] ?? null;
  }

  private function getPermissions() {
    return $this->getPayload()->permissions;
  }
}