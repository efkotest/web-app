<?php

class Api {

  public function request($route) {
    $url = API_SERVER_URL . $route;
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Authorization: ' . $this->getAuthorizationToken()
    ));

    curl_setopt($ch, CURLOPT_URL, $url);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $output = curl_exec($ch);

    curl_close($ch);
    return json_decode($output, true);
  }

  private function getAuthorizationToken() {
    return $_COOKIE['eftt_token'] ?? null;
  }

}