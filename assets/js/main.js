window.vacationsList_API_REQUEST_TYPE = 'PATCH';

$(function() {
  $('#authorizationForm').submit((e) => {
    e.preventDefault();
    authorization();
  });
  $('#vacationRequestForm').submit((e) => {
    e.preventDefault();
    setTimeout(vacationsList , 300);
  });
  $("#vacationRequestModal").on('shown.bs.modal', (e) => {
    window.vacationsList_API_REQUEST_TYPE = $(e.relatedTarget).data('type')
  });
});

function authorization() {
  $.ajax({
    url: API_SERVER_URL + '/authorization',
    type: 'POST',
    data:{
      login: $('#userLogin').val(),
      password: $('#userPassword').val()
    },
    dataType: 'json',
    success: (response) => {
      const Alert = $('#authorizationAlert');
      Alert.hide();
      Alert.removeClass();
      Alert.addClass('alert');
      Alert.find('p').html(response.message);
      if (response.error) {
        Alert.addClass('alert-danger');
      }
      else {
        Alert.addClass('alert-success');
        localStorage.setItem('token', response.token);
        document.cookie = 'eftt_token=' + response.token;
        setTimeout(() => {
          window.location = '/vacations'
        }, 1500);
      }
      Alert.show();
    },
    error: (e) => {
      alert('Прозошла ошибка, пожалуйста сообщите администратору!' + JSON.stringify(e));
    }
  });
}

function vacationsList() {
  let dataRange = $('#datetimepicker').val().split(' - ');
  $.ajax({
    url: API_SERVER_URL + '/vacations/list',
    type: vacationsList_API_REQUEST_TYPE,
    headers: {
      Authorization: localStorage.getItem('token')
    },
    data:{
      startDate: dataRange[0],
      endDate: dataRange[1]
    },
    dataType: 'json',
    success: (response) => {
      const Alert = $('#vacationRequestModalAlert');
      Alert.hide();
      Alert.removeClass();
      Alert.addClass('alert');
      Alert.find('p').html(response.message);
      if (response.error) {
        Alert.addClass('alert-danger');
      }
      else {
        Alert.addClass('alert-success');
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
      Alert.show();
    },
    error: (e) => {
      alert('Прозошла ошибка, пожалуйста сообщите администратору!' + JSON.stringify(e));
    }
  });
}

function updateVacationStatus(id, status) {
  let word = '';
  switch (status) {
    case 2:
      word = 'одобрить';
      break;
    case 1:
      word = 'отклонить';
      break;
    default:
      return false;
  }
  let message = `Вы действительно хотите ${word} заявку на отпуск #${id}?`;
  if (confirm(message) === true) {
    // if press ok, send request to api
    $.ajax({
      url: API_SERVER_URL + '/vacation',
      type: vacationsList_API_REQUEST_TYPE,
      headers: {
        Authorization: localStorage.getItem('token')
      },
      data:{
        id,
        status
      },
      dataType: 'json',
      success: (response) => {
        console.warn(response);
        const Alert = $('#vacationsTableAlert');
        Alert.hide();
        Alert.removeClass();
        Alert.addClass('alert');
        Alert.find('p').html(response.message);
        if (response.error) {
          Alert.addClass('alert-danger');
        }
        else {
          Alert.addClass('alert-success');
          setTimeout(() => {
            window.location.reload();
          }, 1500);
        }
        setTimeout(() => {
          Alert.show();
        }, 100);
      },
      error: (e) => {
        alert('Прозошла ошибка, пожалуйста сообщите администратору!' + JSON.stringify(e));
      }
    });
  }

}

function logout() {
  localStorage.removeItem('token');
  document.cookie = name + 'eftt_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}