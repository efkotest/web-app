<?php
/**
 * Set private property to true if need verify user authorization
 */
$GLOBALS['PAGES_CONFIG'] = [
  'vacations' => [
    'private' => true
  ],
  'authorization' => [
    'private' => false
  ]
];