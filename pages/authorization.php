<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-xs-offset-0 col-sm-offset-4 col-md-offset-4 col-lg-offset-4">
  <form class="form-signin" id="authorizationForm">
    <h2 class="form-signin-heading">Авторизация</h2>
    <div class="alert" id="authorizationAlert" style="display: none;">
      <button type="button" class="close" data-dismiss="alert"
              aria-hidden="true">&times;
      </button>
      <p></p>
    </div>
    <div class="form-group">
      <label for="userLogin">
        <span class="glyphicon glyphicon-user"></span>
        Email адрес
      </label>
      <input
        type="email"
        id="userLogin"
        class="form-control"
        placeholder="worker@company.com"
        required
        autofocus
      />
    </div>
    <div class="form-group">
      <label for="userPassword">
        <span class="glyphicon glyphicon-lock"></span>
        Пароль
      </label>
      <input
        type="password"
        id="userPassword"
        class="form-control"
        placeholder="Пароль"
        required
      />
    </div>

    <button class="btn btn-lg btn-primary btn-block" type="submit">
      Авторизоваться
    </button>
  </form>

</div>