<?php
require_once(dirname(__FILE__).'/../autoloader.php');

$Api = new Api();
$User = new User();

$myVacationStatus = $Api->request('/vacation/status') ?? [];
$vacationsList = $Api->request('/vacations/list') ?? [];
$jwtPayload = $User->getPayload();

$vacationsListHTML = "";
for ($i = 0; $i < sizeof($vacationsList['items']); $i++) {
  $item = $vacationsList['items'][$i];

  $idColumn = '';
  $manageColumn = '';
  if ($User->isAbbleTo('vacationsManage')) {
    $idColumn = "<td>{$item['id']}</td>";
    $manageColumn = '<td align="center">
      <button
        class="btn btn-success btn-xs"
        onclick="updateVacationStatus('.$item['id'].', 2)"
      >
        <i class="glyphicon glyphicon-ok"></i>
      </button>
      <button
        class="btn btn-danger btn-xs"
        onclick="updateVacationStatus('.$item['id'].', 1)"
      >
        <i class="glyphicon glyphicon-remove"></i>
      </button>
    </td>';
  }
  $status = '<i
      class="glyphicon glyphicon-hourglass"
      data-toggle="tooltip"
      data-placement="top"
      title=""
      data-original-title="В процессе рассмотрения.."
    ></i>';
  if ((int) $item['status'] === 2) {
    $status = '<i
      class="glyphicon glyphicon-ok-sign text-success"
      style="font-size: 17pt;"
      data-toggle="tooltip"
      data-placement="top"
      title=""
      data-original-title="Подтверждено руководителем"
    ></i>';
    $manageColumn = '<td></td>';
  }
  else if ((int) $item['status'] === 1) {
    $status = '<i
      class="glyphicon glyphicon-remove text-danger"
      data-toggle="tooltip"
      data-placement="top"
      title=""
      data-original-title="Отклонено руководителем"
    ></i>';
    $manageColumn = '<td></td>';
  }
  $vacationsListHTML .= "
    <tr>
      {$idColumn}
      <td>{$item['firstName']} {$item['lastName']}</td>
      <td align='center'>с <b>{$item['startDate']}</b> по <b>{$item['endDate']}</b></td>
      <td align='center'>{$status}</td>
      {$manageColumn}
    </tr>
  ";
}
?>

<div class="col-xs-12 col-sm-12 col-md-10 col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-2">

  <div class="jumbotron">
    <div class="container">
      <h3>
        Добро пожаловать, <?php echo $jwtPayload->firstName .
        ' ' . $jwtPayload->lastName; ?>!
      </h3>
      <?php
      if ($User->isAbbleTo('vacationPlanning')) {
        if ($myVacationStatus['id']) {
          if ($myVacationStatus['status'] === 2) {
            echo '<p class="text-success">
              Даты вашего отпуска одобрены руководителем!
            </p>';
          }
          else if ($myVacationStatus['status'] === 1) {
            echo '<p class="text-danger">
              Даты вашего отпуска отклонены руководителем!
              Вы можете изменить их.
            </p>';
          }
          else {
            echo '<p>
              Даты вашего отпуска еще не подтверждены руководителем.
              Вы можете изменить их.
            </p>';
          }
        }
        else {
          echo '<p>
            Вы можете скорректировать даты своего отпуска с
            вашими коллегами и отправить на подтверждение руководителю.
          </p>';
        }
      }
      if ($User->isAbbleTo('vacationsManage')) {
        echo '<p>
          Вы можете модерировать заявки на отпуск своих сотрудников
          отклоняя или подтверждая их.
        </p>';
      }
      ?>
      <p>
        <?php
        if ($User->isAbbleTo('vacationPlanning')) {
          if ($myVacationStatus['id']) {
            if ($myVacationStatus['status'] !== 2) {
              echo '<a class="btn btn-primary btn-sm" data-toggle="modal" href="#vacationRequestModal" data-type="PATCH">
                Изменить даты отпуска
              </a>';
            }
          }
          else {
            echo '<a class="btn btn-primary btn-sm" data-toggle="modal" href="#vacationRequestModal" data-type="POST">
              Запросить отпуск
            </a>';
          }
        }
        ?>
        <a class="btn btn-default btn-sm pull-right" onclick="logout()" href="/">
          Выйти
          <i class="glyphicon glyphicon-log-out"></i>
        </a>
      </p>
    </div>
  </div>

  <div class="alert" id="vacationsTableAlert" style="display: none;">
    <button type="button" class="close" data-dismiss="alert"
            aria-hidden="true">&times;
    </button>
    <p></p>
  </div>

  <table class="table table-hover table-responsive">
    <thead>
    <tr>
      <?php
      if ($User->isAbbleTo('vacationsManage')) {
        echo '<th>#ID</th>';
      }
      ?>
      <th>Сотрудник</th>
      <th style="text-align: center;">Отпуск</th>
      <th style="text-align: center;">Статус</th>
      <?php
      if ($User->isAbbleTo('vacationsManage')) {
        echo '<th style="text-align: center;">Управление</th>';
      }
      ?>
    </tr>
    </thead>
    <tbody>
    <?php echo $vacationsListHTML; ?>
    </tbody>
  </table>

  <div class="modal fade" id="vacationRequestModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock"></span> Отпуск</h4>
        </div>
        <div class="modal-body">
          <div class="alert" id="vacationRequestModalAlert" style="display: none;">
            <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true">&times;
            </button>
            <p></p>
          </div>
          <form role="form" id="vacationRequestForm">
            <div class="form-group">
              <label for="datetimepicker">
                <span class="glyphicon glyphicon-user"></span>
                Укажите желаемые даты
              </label>
              <div class="input-group date">
                <input
                  id="datetimepicker"
                  type="text"
                  class="form-control"
                  placeholder="Нажмите чтобы выбрать"
                />
                <span class="input-group-addon" id="datetimepickerbtn" style="cursor: pointer;">
                  <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="$('#vacationRequestForm').submit()">
            Отправить
          </button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">
            Отменить
          </button>
        </div>
      </div>
    </div>
  </div>

</div>

<script type="text/javascript">
  $(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('#datetimepicker').daterangepicker({
      minDate: moment().format('DD.MM.YYYY'),
      startDate: moment().format('DD.MM.YYYY'),
      locale: {
        applyLabel: 'Принять',
        cancelLabel: 'Отменить'
      }
    });
    $('#datetimepickerbtn').on('click', () => {
      $('#datetimepicker').data('daterangepicker').show();
    })
  });
</script>